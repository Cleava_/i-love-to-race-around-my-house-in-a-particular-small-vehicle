// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "MicroMachine_FullGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MICROMACHINES_API AMicroMachine_FullGameMode : public AGameMode
{
	GENERATED_BODY()
	
};
