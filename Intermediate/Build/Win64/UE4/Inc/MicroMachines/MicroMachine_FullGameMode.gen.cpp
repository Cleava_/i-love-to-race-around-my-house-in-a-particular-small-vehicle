// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MicroMachines/MicroMachine_FullGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMicroMachine_FullGameMode() {}
// Cross Module References
	MICROMACHINES_API UClass* Z_Construct_UClass_AMicroMachine_FullGameMode_NoRegister();
	MICROMACHINES_API UClass* Z_Construct_UClass_AMicroMachine_FullGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameMode();
	UPackage* Z_Construct_UPackage__Script_MicroMachines();
// End Cross Module References
	void AMicroMachine_FullGameMode::StaticRegisterNativesAMicroMachine_FullGameMode()
	{
	}
	UClass* Z_Construct_UClass_AMicroMachine_FullGameMode_NoRegister()
	{
		return AMicroMachine_FullGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AMicroMachine_FullGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMicroMachine_FullGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameMode,
		(UObject* (*)())Z_Construct_UPackage__Script_MicroMachines,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMicroMachine_FullGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MicroMachine_FullGameMode.h" },
		{ "ModuleRelativePath", "MicroMachine_FullGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMicroMachine_FullGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMicroMachine_FullGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMicroMachine_FullGameMode_Statics::ClassParams = {
		&AMicroMachine_FullGameMode::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009002ACu,
		nullptr, 0,
		nullptr, 0,
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AMicroMachine_FullGameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMicroMachine_FullGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMicroMachine_FullGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMicroMachine_FullGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMicroMachine_FullGameMode, 1789934331);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMicroMachine_FullGameMode(Z_Construct_UClass_AMicroMachine_FullGameMode, &AMicroMachine_FullGameMode::StaticClass, TEXT("/Script/MicroMachines"), TEXT("AMicroMachine_FullGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMicroMachine_FullGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
