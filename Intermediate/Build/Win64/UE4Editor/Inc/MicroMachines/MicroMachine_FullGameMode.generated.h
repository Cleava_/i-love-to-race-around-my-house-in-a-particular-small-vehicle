// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MICROMACHINES_MicroMachine_FullGameMode_generated_h
#error "MicroMachine_FullGameMode.generated.h already included, missing '#pragma once' in MicroMachine_FullGameMode.h"
#endif
#define MICROMACHINES_MicroMachine_FullGameMode_generated_h

#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_RPC_WRAPPERS
#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMicroMachine_FullGameMode(); \
	friend struct Z_Construct_UClass_AMicroMachine_FullGameMode_Statics; \
public: \
	DECLARE_CLASS(AMicroMachine_FullGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MicroMachines"), NO_API) \
	DECLARE_SERIALIZER(AMicroMachine_FullGameMode)


#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMicroMachine_FullGameMode(); \
	friend struct Z_Construct_UClass_AMicroMachine_FullGameMode_Statics; \
public: \
	DECLARE_CLASS(AMicroMachine_FullGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MicroMachines"), NO_API) \
	DECLARE_SERIALIZER(AMicroMachine_FullGameMode)


#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMicroMachine_FullGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMicroMachine_FullGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMicroMachine_FullGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMicroMachine_FullGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMicroMachine_FullGameMode(AMicroMachine_FullGameMode&&); \
	NO_API AMicroMachine_FullGameMode(const AMicroMachine_FullGameMode&); \
public:


#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMicroMachine_FullGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMicroMachine_FullGameMode(AMicroMachine_FullGameMode&&); \
	NO_API AMicroMachine_FullGameMode(const AMicroMachine_FullGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMicroMachine_FullGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMicroMachine_FullGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMicroMachine_FullGameMode)


#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_PRIVATE_PROPERTY_OFFSET
#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_12_PROLOG
#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_RPC_WRAPPERS \
	MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_INCLASS \
	MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_INCLASS_NO_PURE_DECLS \
	MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MicroMachesRepo_Source_MicroMachines_MicroMachine_FullGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
